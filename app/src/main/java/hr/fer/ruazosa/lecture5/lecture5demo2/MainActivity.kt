package hr.fer.ruazosa.lecture5.lecture5demo2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider

import hr.fer.ruazosa.lecture5.lecture5demo2.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val myViewModel = ViewModelProvider(this)[MyViewModel::class.java]

        myViewModel.resultOfDataFetch.observe(this)  {
            binding.operationStatusTextView.text = it
        }

        binding.startLongRunningOperationButton.setOnClickListener {
            myViewModel.fetchDataFromRepository()
        }

        binding.sayHelloButton.setOnClickListener {
            val toastMessage = Toast.makeText(applicationContext, "Hello world", Toast.LENGTH_LONG)
            toastMessage.show()
        }

    }

}